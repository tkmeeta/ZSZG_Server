package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CardData implements PropertyReader {
	
	public int id;
	
	public int number;
	
	public String name;
	
	public String description;
	
	public int star;
	
	public int maxLevel;
	
	public int race;
	
	public int element;
	
	//==蓄力声音==//
	public String chargeVoice;
	//==受伤声音==//
	public String hurtVoice;
	
	public float atk;
	
	public float def;
	
	public float hp;
	
	public int criRate;
	
	public int aviRate;
	
	public int talent;
	
	public int talent2;
	public int talent3;
	
	public float talentpower;
	
	public int type;
	
	public float castTimeDelay;
	
	public int shadow;
	
	public String atlas;
	
	public String cardmodel;
	
	public float modelsize;
	
	public float modelposition;
	
	public float scalenum;
	
	public float modelrotation;
	
	public int sex;
	
	public String icon;
	
	public int sell;
	
	public int basicskill;
	
	public int exp;
	
	public int level;
	public String waytoget;
	
	
	private static HashMap<Integer, CardData> data = new HashMap<Integer, CardData>();
	private static List<CardData> dataList=new ArrayList<CardData>();
	
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}
	
	public void resetData()
	{

		data.clear();
	}
	
	public void parse(String[] ss)
	{

	}
	
	public static CardData getData(int cardId)
	{

		return data.get(cardId);
	}
	
	/** ****所有卡牌**** */
	
	public static List<CardData> getAllCardDatas()
	{
		return dataList;
	}
}
