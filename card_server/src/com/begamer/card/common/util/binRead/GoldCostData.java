package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GoldCostData implements PropertyReader
{
	public int id;
	public int number;
	public int viplevel;
	public int type;
	public int cost;
	
	private static HashMap<Integer,GoldCostData> data =new HashMap<Integer, GoldCostData>();
	private static List<GoldCostData> dataList =new ArrayList<GoldCostData>();
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	/**根据id获取data**/
	public static GoldCostData getGoldCostData(int index)
	{
		return data.get(index);
	}
	/**获取data长度**/
	public static Integer getDataSize()
	{
		return data.size();
	}
	
	//根据vip等级获取data长度
	public static Integer getMaxSize(int viplv)
	{
		for(int i=0;i<dataList.size();i++)
		{
			if(dataList.get(i).viplevel>viplv)
			{
				return i+1;
			}
		}
		return -1;
	}
}
