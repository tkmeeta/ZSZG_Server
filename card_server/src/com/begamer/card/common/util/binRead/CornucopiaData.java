package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class CornucopiaData implements PropertyReader
{
	public int id;
	public int cost;
	public List<String> dayaward;
	
	private static HashMap<Integer, CornucopiaData> data =new HashMap<Integer, CornucopiaData>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id =StringUtil.getInt(ss[location]);
		cost =StringUtil.getInt(ss[location+1]);
		dayaward =new ArrayList<String>();
		for(int i=0;i<8;i++)
		{
			location =2+i*2;
			int awardType =StringUtil.getInt(ss[location]);
			String awardId =StringUtil.getString(ss[location+1]);
			String award = awardType+"-"+awardId;
			if(awardType !=0)
			{
				dayaward.add(award);
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static CornucopiaData getCornucopiaData(int index)
	{
		return data.get(index);
	}
}
