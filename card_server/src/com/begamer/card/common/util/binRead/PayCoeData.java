package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.List;

public class PayCoeData implements PropertyReader
{
	public int payed;
	public float coefficient;

	private static List<PayCoeData> data=new ArrayList<PayCoeData>();
	
	@Override
	public void addData()
	{
		data.add(0, this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	/**
	 * 根据已付的费用获取付费系数
	 * lt@2014-1-20 下午03:40:42
	 * @param payNum
	 * @return
	 */
	public static float getPayMul(int payNum)
	{
		for(int i=0;i<data.size();i++)
		{
			PayCoeData pcd=data.get(i);
			if(payNum>=pcd.payed)
			{
				return pcd.coefficient;
			}
		}
		return 1;
	}
	
}
