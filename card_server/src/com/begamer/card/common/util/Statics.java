package com.begamer.card.common.util;

import java.util.ArrayList;
import java.util.List;

import com.begamer.card.cache.Cache;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.binRead.BagCostData;
import com.begamer.card.common.util.binRead.CardData;
import com.begamer.card.common.util.binRead.CardDropData;
import com.begamer.card.common.util.binRead.CardPropertyData;
import com.begamer.card.common.util.binRead.EquipData;
import com.begamer.card.common.util.binRead.EquippropertyData;
import com.begamer.card.common.util.binRead.EvolutionData;
import com.begamer.card.common.util.binRead.FBeventData;
import com.begamer.card.common.util.binRead.FriendOddData;
import com.begamer.card.common.util.binRead.MazeBattleData;
import com.begamer.card.common.util.binRead.MazeProbabilityData;
import com.begamer.card.common.util.binRead.MazeSkillDropData;
import com.begamer.card.common.util.binRead.MissionData;
import com.begamer.card.common.util.binRead.PassiveSkillData;
import com.begamer.card.common.util.binRead.PowerData;
import com.begamer.card.common.util.binRead.RacePowerData;
import com.begamer.card.common.util.binRead.RuneData;
import com.begamer.card.common.util.binRead.RuneTotalData;
import com.begamer.card.common.util.binRead.SkillData;
import com.begamer.card.common.util.binRead.SkillPropertyData;
import com.begamer.card.common.util.binRead.TalentData;
import com.begamer.card.common.util.binRead.UnitSkillData;
import com.begamer.card.common.util.binRead.WishingData;
import com.begamer.card.json.PackElement;
import com.begamer.card.json.command2.BattleLogJson;
import com.begamer.card.json.command2.BattleResultJson;
import com.begamer.card.model.pojo.Card;
import com.begamer.card.model.pojo.CardGroup;
import com.begamer.card.model.pojo.Equip;
import com.begamer.card.model.pojo.EventDrop;
import com.begamer.card.model.pojo.Item;
import com.begamer.card.model.pojo.PassiveSkill;
import com.begamer.card.model.pojo.Skill;

public class Statics {
	public static float getCardSelfMaxHp(int cardId, int level, int breakNum)
	{
		CardData cd = CardData.getData(cardId);
		if (cd == null)
		{
			return 0;
		}
		CardPropertyData cpd = CardPropertyData.getData(level);
		if (cpd == null)
		{
			return 0;
		}
		EvolutionData ed = EvolutionData.getData(cd.star, breakNum);
		float mul = 0;
		if (ed != null)
		{
			mul = ed.status;
		}
		return cd.hp / 5 * cpd.hp * (1 + 0.01f * mul);
	}
	
	/** 获取卡本身的最大atk **/
	public static float getCardSelfMaxAtk(int cardId, int level, int breakNum)
	{
		CardData cd = CardData.getData(cardId);
		if (cd == null)
		{
			return 0;
		}
		CardPropertyData cpd = CardPropertyData.getData(level);
		if (cpd == null)
		{
			return 0;
		}
		EvolutionData ed = EvolutionData.getData(cd.star, breakNum);
		float mul = 0;
		if (ed != null)
		{
			mul = ed.status;
		}
		return cd.atk / 5 * cpd.atk * (1 + 0.01f * mul);
	}
	
	/** 获取卡本身的最大def **/
	public static float getCardSelfMaxDef(int cardId, int level, int breakNum)
	{
		CardData cd = CardData.getData(cardId);
		if (cd == null)
		{
			return 0;
		}
		CardPropertyData cpd = CardPropertyData.getData(level);
		if (cpd == null)
		{
			return 0;
		}
		EvolutionData ed = EvolutionData.getData(cd.star, breakNum);
		float mul = 0;
		if (ed != null)
		{
			mul = ed.status;
		}
		return cd.def / 5 * cpd.def * (1 + 0.01f * mul);
	}
	
	/**
	 * 获取符文属性 lt@2014-2-21 上午11:22:16
	 * 
	 * @param runeId
	 *            当前符文Id
	 * @param property
	 *            1atk,2def,3hp,4cri,5avi
	 * @return
	 */
	private static int getRuneValue(String runeId, int property)
	{
		if (runeId == null || "".equals(runeId.trim()))
		{
			return 0;
		}
		int result = 0;
		String[] ss = runeId.split("-");
		int times = StringUtil.getInt(ss[0]);
		int[] pageNums = new int[Constant.RunePageLength];
		// 计算前几轮的数据
		for (int k = 0; k < times; k++)
		{
			result += RuneTotalData.getValues(k, property);
		}
		// 计算当前轮的数据
		for (int k = 0; k < pageNums.length; k++)
		{
			pageNums[k] = StringUtil.getInt(ss[k + 1]);
			int key = times * 100 + k + 1;
			RuneTotalData rdt = RuneTotalData.getData(key);
			if (rdt.proprety == property)
			{
				result += RuneData.getValues(key, pageNums[k]);
				if (pageNums[k] == RuneData.getGroupDataNum(key))
				{
					result += rdt.value;
				}
			}
		}
		return result;
	}
	
	// ==获取暴击率==//
	private static int getCriRate(int cardId, int skillId, List<Integer> pSkillIds, String runeId, int talent1, int talent2, int selfIndex, int[] teamerCardIds, int[] talents1, int[] talents2)
	{
		float cri = 0;
		if (pSkillIds != null && pSkillIds.size() > 0)
		{
			for (int psid : pSkillIds)
			{
				PassiveSkillData psd = PassiveSkillData.getData(psid);
				if (psd != null && psd.type == 4)
				{
					cri += psd.numbers;
				}
			}
		}
		CardData cd = CardData.getData(cardId);
		SkillData sd = SkillData.getData(skillId);
		cri += getTalentB_cri(talent1, sd) + getTalentB_cri(talent2, sd);
		cri += getTalentC_cri(cd, selfIndex, teamerCardIds, talents1) + getTalentC_cri(cd, selfIndex, teamerCardIds, talents2);
		// 符文//
		cri += getRuneValue(runeId, 4);
		// ==卡自带暴击==//
		cri += cd.criRate;
		
		cri = cri > 100 ? 100 : cri;
		return (int) cri;
	}
	
	private static float getTalentB_cri(int talent, SkillData sd)
	{
		float cri = 0;
		// ==天赋:1对技能类型加成==//
		TalentData td = TalentData.getData(talent);
		if (sd != null && td != null && td.type == 1 && td.class1 == sd.type && td.effect == 4)
		{
			cri = td.number;
		}
		// ==天赋:4对特定技能==//
		if (sd != null && td != null && td.type == 4 && td.class1 / 10 == sd.index / 10 && td.effect == 5)
		{
			cri = td.number;
		}
		return cri;
	}
	
	private static float getTalentC_cri(CardData cd, int selfIndex, int[] teamerCardIds, int[] talents)
	{
		float criAdd = 0;
		if (selfIndex != -1)
		{
			// ==队友天赋加成:5对本方单位==//
			for (int k = 0; k < teamerCardIds.length; k++)
			{
				int cId = teamerCardIds[k];
				if (cId != 0)
				{
					CardData cdTemp = CardData.getData(cId);
					int talent = talents[k];
					if (talent == 0)
					{
						continue;
					}
					TalentData td2 = TalentData.getData(talent);
					if (td2 != null && (td2.type == 5 || td2.type == 51) && td2.effect == 4)
					{
						switch (td2.class1)
						{
							case 1:// 本种族卡牌//
								if (cd.race == cdTemp.race)
								{
									if (td2.number > criAdd)
									{
										criAdd = td2.number;
									}
								}
								break;
							case 2:// 固定前排//
								if (selfIndex < 3)
								{
									if (td2.number > criAdd)
									{
										criAdd = td2.number;
									}
								}
								break;
							case 3:// 固定后排//
								if (selfIndex >= 3)
								{
									if (td2.number > criAdd)
									{
										criAdd = td2.number;
									}
								}
								break;
							case 4:// 前排对位//
								if (selfIndex + 3 == k)
								{
									if (td2.number > criAdd)
									{
										criAdd = td2.number;
									}
								}
								break;
							case 5:// 所有卡牌//
								if (td2.number > criAdd)
								{
									criAdd = td2.number;
								}
								break;
							default:// 固定卡牌//
								if (td2.class1 == cd.id)
								{
									if (td2.number > criAdd)
									{
										criAdd = td2.number;
									}
								}
								break;
						}
					}
				}
			}
		}
		return criAdd;
	}
	
	// ==获取暴击率(UI使用)==//
	private static int getCriRate(Card self, int skillId, List<Integer> pSkillIds, CardGroup cg, String runeId)
	{
		int talent1 = self.getTalent1();
		int talent2 = self.getTalent2();
		int selfIndex = -1;
		int[] teamerCardIds = null;
		int[] talents1 = null;
		int[] talents2 = null;
		if (cg != null)
		{
			selfIndex = getIndexInArray(self, cg.cards);
			teamerCardIds = new int[6];
			talents1 = new int[6];
			talents2 = new int[6];
			for (int i = 0; i < cg.cards.length; i++)
			{
				if (cg.cards[i] != null)
				{
					teamerCardIds[i] = cg.cards[i].getCardId();
					talents1[i] = cg.cards[i].getTalent1();
					talents2[i] = cg.cards[i].getTalent2();
				}
			}
		}
		return getCriRate(self.getCardId(), skillId, pSkillIds, runeId, talent1, talent2, selfIndex, teamerCardIds, talents1, talents2);
	}
	
	// ==获取闪避率==//
	private static int getAviRate(int cardId, int skillId, List<Integer> pSkillIds, String runeId, int talent1, int talent2, int selfIndex, int[] teamerCardIds, int[] talents1, int[] talents2)
	{
		float avi = 0;
		if (pSkillIds != null && pSkillIds.size() > 0)
		{
			for (int psid : pSkillIds)
			{
				PassiveSkillData psd = PassiveSkillData.getData(psid);
				if (psd != null && psd.type == 5)
				{
					avi += psd.numbers;
				}
			}
		}
		CardData cd = CardData.getData(cardId);
		SkillData sd = SkillData.getData(skillId);
		avi += getTalentB_avi(talent1, sd) + getTalentB_avi(talent2, sd);
		avi += getTalentC_avi(cd, selfIndex, teamerCardIds, talents1) + getTalentC_avi(cd, selfIndex, teamerCardIds, talents2);
		// 符文//
		avi += getRuneValue(runeId, 5);
		// ==卡自带闪避==//
		avi += cd.aviRate;
		
		avi = avi > 100 ? 100 : avi;
		return (int) avi;
	}
	
	private static float getTalentB_avi(int talent, SkillData sd)
	{
		float avi = 0;
		// ==天赋:1对技能类型加成==//
		TalentData td = TalentData.getData(talent);
		if (sd != null && td != null && td.type == 1 && td.class1 == sd.type && td.effect == 5)
		{
			avi = td.number;
		}
		// ==天赋:4对特定技能==//
		if (sd != null && td != null && td.type == 4 && td.class1 / 10 == sd.index / 10 && td.effect == 4)
		{
			avi = td.number;
		}
		return avi;
	}
	
	private static float getTalentC_avi(CardData cd, int selfIndex, int[] teamerCardIds, int[] talents)
	{
		float aviAdd = 0;
		if (selfIndex != -1)
		{
			// ==队友天赋加成:5对本方单位==//
			for (int k = 0; k < teamerCardIds.length; k++)
			{
				int cId = teamerCardIds[k];
				if (cId != 0)
				{
					CardData cdTemp = CardData.getData(cId);
					int talent = talents[k];
					if (talent == 0)
					{
						continue;
					}
					TalentData td2 = TalentData.getData(talent);
					if (td2 != null && (td2.type == 5 || td2.type == 51) && td2.effect == 5)
					{
						switch (td2.class1)
						{
							case 1:// 本种族卡牌//
								if (cd.race == cdTemp.race)
								{
									if (td2.number > aviAdd)
									{
										aviAdd = td2.number;
									}
								}
								break;
							case 2:// 固定前排//
								if (selfIndex < 3)
								{
									if (td2.number > aviAdd)
									{
										aviAdd = td2.number;
									}
								}
								break;
							case 3:// 固定后排//
								if (selfIndex >= 3)
								{
									if (td2.number > aviAdd)
									{
										aviAdd = td2.number;
									}
								}
								break;
							case 4:// 前排对位//
								if (selfIndex + 3 == k)
								{
									if (td2.number > aviAdd)
									{
										aviAdd = td2.number;
									}
								}
								break;
							case 5:// 所有卡牌//
								if (td2.number > aviAdd)
								{
									aviAdd = td2.number;
								}
								break;
							default:// 固定卡牌//
								if (td2.class1 == cd.id)
								{
									if (td2.number > aviAdd)
									{
										aviAdd = td2.number;
									}
								}
								break;
						}
					}
				}
			}
		}
		return aviAdd;
	}
	
	// ==获取闪避率(UI使用)==//
	private static int getAviRate(Card self, int skillId, List<Integer> pSkillIds, CardGroup cg, String runeId)
	{
		int talent1 = self.getTalent1();
		int talent2 = self.getTalent2();
		int selfIndex = -1;
		int[] teamerCardIds = null;
		int[] talents1 = null;
		int[] talents2 = null;
		if (cg != null)
		{
			selfIndex = getIndexInArray(self, cg.cards);
			teamerCardIds = new int[6];
			talents1 = new int[6];
			talents2 = new int[6];
			for (int i = 0; i < cg.cards.length; i++)
			{
				if (cg.cards[i] != null)
				{
					teamerCardIds[i] = cg.cards[i].getCardId();
					talents1[i] = cg.cards[i].getTalent1();
					talents2[i] = cg.cards[i].getTalent2();
				}
			}
		}
		return getAviRate(self.getCardId(), skillId, pSkillIds, runeId, talent1, talent2, selfIndex, teamerCardIds, talents1, talents2);
	}
	
	// **获取装备数值**//
	private static int getEquipValue(EquipData ed, int equipLevel)
	{
		EquippropertyData equippropertyData = EquippropertyData.getData(ed.type, equipLevel);
		return equippropertyData.starNumbers[ed.star - 1];
	}
	
	private static int getIndexInArray(Card c, Card[] cs)
	{
		int index = -1;
		for (int k = 0; k < cs.length; k++)
		{
			if (cs[k] == c)
			{
				index = k;
				break;
			}
		}
		return index;
	}
	
	/** 校验战斗日志 **/
	public static boolean checkBattleLog(BattleResultJson brj, BattleLogJson blj)
	{
		if (blj.bs != null)
		{
			for (String s : blj.bs)
			{
				if (s.contains("-"))
				{
					String[] ss = s.split("-");
					if (ss.length == 3)
					{
						int unitId = StringUtil.getInt(ss[0]);
						int cost = StringUtil.getInt(ss[1]);
						int maxEnergy = StringUtil.getInt(ss[2]);
						UnitSkillData usd = UnitSkillData.getData(unitId);
						if (usd == null)
						{
							return false;
						}
						if (usd.cost != cost)
						{
							return false;
						}
						if (brj.mes[0] != maxEnergy)
						{
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	
	private static int getTalentProAdd(CardGroup cg)
	{
		// ==天赋:特殊,增加稀有道具(mission表里掉落的最后一列)掉落概率==//
		int bestProAdd = 0;
		for (Card c : cg.cards)
		{
			if (c != null)
			{
				TalentData td = TalentData.getData(c.getTalent1());
				if (td != null && td.type == 7 && td.class1 == 6)
				{
					bestProAdd += Constant.TotalDropPro * td.number;
				}
				TalentData td2 = TalentData.getData(c.getTalent2());
				if (td2 != null && td2.type == 7 && td2.class1 == 6)
				{
					bestProAdd += Constant.TotalDropPro * td2.number;
				}
			}
		}
		return bestProAdd;
	}
	
	// 计算掉落物品，并将掉落物品怪物位置，掉落物品的星级和类型发给客户端
	public static String[] dropsThings(int targetId, CardGroup cg, PlayerInfo pi)
	{
		int bestProAdd = getTalentProAdd(cg);
		MissionData missionData = MissionData.getMissionData(targetId);
		if (missionData != null)
		{
			String[] dropsInfo = null;
			String[] monsters = missionData.monsters;
			if (monsters != null && monsters.length > 0)
			{
				dropsInfo = new String[monsters.length];
				List<String> drops = missionData.drops;
				for (int i = 0; i < monsters.length; i++)
				{
					// 新手第一次打1-10必得双刃剑
					if (i == 0 && targetId == 110203 && pi.player.getNewPlayerType() == 13)
					{
						dropsInfo[0] = 2 + "-" + "1101";
					}
					else
					{
						if (monsters[i] != null)
						{
							String monsterInfo = monsters[i].split("-")[0];
							if (Integer.parseInt(monsterInfo) > 0)
							{
								if (drops != null && drops.size() > 0)
								{
									int random = Random.getNumber(0, Constant.TotalDropPro);
									random = (int) (random / Cache.getMissionDropMul());
									random -= bestProAdd;
									// 从全局掉落里得
									/**
									 * card,level item,num skill pskill equip
									 * **/
									List<EventDrop> eds = null;
									if (missionData.missiontype == 1)
									{
										eds = Cache.eventDropsForNormal;
									}
									else
									{
										eds = Cache.eventDropsForSpecial;
									}
									if (eds != null)
									{
										for (EventDrop ed : eds)
										{
											if (dropsInfo[i] == null)
											{
												for (String s : ed.getRewards())
												{
													if (s != null)
													{
														String[] ss = s.split("&");
														int type = StringUtil.getInt(ss[0]);
														int id = StringUtil.getInt(ss[1]);
														int num = StringUtil.getInt(ss[2]);
														int pro = StringUtil.getInt(ss[3]);
														if (random < pro)
														{
															String msg = null;
															switch (type)
															{
																case 1:// item
																	msg = type + "-" + id + "," + num;
																	break;
																case 2:// equip
																case 4:// skill
																case 5:// pskill
																	msg = type + "-" + id;
																	break;
																case 3:// card
																	msg = type + "-" + id + ",1";
																	break;
															}
															dropsInfo[i] = msg;
															break;
														}
														random -= pro;
													}
												}
											}
											else
											{
												break;
											}
										}
									}
									if (dropsInfo[i] != null)
									{
										continue;
									}
									// 从mission表里得
									for (int j = 0; j < drops.size(); j++)
									{
										String drop = drops.get(j);
										String[] ss = drop.split("-");
										int dropsType = StringUtil.getInt(ss[0]);
										String dropInfo = ss[1];
										int pro = StringUtil.getInt(ss[2]);
										if (random < pro)
										{
											if (dropsType == 3)// 根据掉落序列id随机出一个物品
											{
												int num = Random.getNumber(0, 1000);
												String[] temp = dropInfo.split(",");
												List<CardDropData> cds = CardDropData.getRandCardDropDatas(StringUtil.getInt(temp[0]));
												int st = 0;
												String cId = "";
												for (int k = 0; k < cds.size(); k++)
												{
													int end = st + cds.get(k).probability;
													if (num > st && num <= end)
													{
														if (cds.get(k).type == 3 || cds.get(k).type == 1)
														{
															cId = cds.get(k).type + "-" + cds.get(k).cardID + "," + temp[1];
														}
														else
														{
															cId = cds.get(k).type + "-" + cds.get(k).cardID;
														}
														break;
													}
													st = end;
												}
												if (cId != null)
												{
													dropsInfo[i] = cId;
												}
											}
											else if (dropsType == 6)
											{
												dropsInfo[i] = 3 + "-" + dropInfo;
											}
											else
											{
												dropsInfo[i] = dropsType + "-" + dropInfo;
											}
											break;
										}
										else
										{
											random -= pro;
										}
									}
								}
							}
						}
					}
				}
			}
			return dropsInfo;
		}
		return null;
	}
	
	// 活动副本：计算掉落物品，并将掉落物品怪物位置，掉落物品的星级和类型发给客户端
	public static String[] drops(int targetId, CardGroup cg)
	{
		FBeventData fData = FBeventData.getFBeventData(targetId);
		int bestProAdd = getTalentProAdd(cg);
		if (fData != null)
		{
			String[] dropsInfo = null;
			String[] monsters = fData.monsters;
			if (monsters != null && monsters.length > 0)
			{
				
				dropsInfo = new String[monsters.length];
				List<String> drops = fData.drops;
				for (int i = 0; i < monsters.length; i++)
				{
					if (i == 0)
					{
						// if(){}
					}
					if (monsters[i] != null)
					{
						String monsterInfo = monsters[i].split("-")[0];
						if (Integer.parseInt(monsterInfo) > 0)
						{
							if (drops != null && drops.size() > 0)
							{
								int random = Random.getNumber(0, Constant.TotalDropPro);
								for (int j = 0; j < drops.size(); j++)
								{
									String drop = drops.get(j);
									String[] ss = drop.split("-");
									int dropsType = StringUtil.getInt(ss[0]);
									String dropInfo = ss[1];
									int pro = StringUtil.getInt(ss[2]);
									// 最后一个元素为稀有物品
									if (j == drops.size() - 1)
									{
										pro += bestProAdd;
									}
									if (random < pro)
									{
										dropsInfo[i] = dropsType + "-" + dropInfo;
										break;
									}
									else
									{
										random -= pro;
									}
								}
							}
						}
					}
				}
			}
			return dropsInfo;
		}
		return null;
	}
	
	/**
	 * 判断指定类型指定位置的物品是否在卡组中 lt@2014-1-10 下午06:57:19
	 * 
	 * @param pi
	 * @param eType
	 *            :1,2,3,4,5
	 * @param index
	 * @return
	 */
	public static boolean isInCardGroup(PlayerInfo pi, int eType, int index)
	{
		return getCardGroup(pi, eType, index) != null;
	}
	
	/**
	 * 根据物品类型和绝对索引获取所在卡组 lt@2014-1-13 下午07:52:35
	 * 
	 * @param pi
	 * @param eType
	 * @param index
	 * @return
	 */
	public static CardGroup getCardGroup(PlayerInfo pi, int eType, int index)
	{
		List<Card> cards = pi.getCards();
		List<Skill> skills = pi.getSkills();
		List<PassiveSkill> pSkills = pi.getPassiveSkillls();
		List<Equip> equips = pi.getEquips();
		
		CardGroup cg = pi.getCardGroup();
		switch (eType)
		{
			case 1:// card
				if (cg.cards != null)
				{
					for (Card c : cg.cards)
					{
						if (c == cards.get(index))
						{
							return cg;
						}
					}
				}
				break;
			case 2:// skill
				if (cg.skills != null)
				{
					for (Skill s : cg.skills)
					{
						if (s == skills.get(index))
						{
							return cg;
						}
					}
				}
				break;
			case 3:// pSkill
				if (cg.pSkills != null)
				{
					for (List<PassiveSkill> ps : cg.pSkills)
					{
						if (ps != null && ps.size() > 0)
						{
							for (PassiveSkill p : ps)
							{
								if (p == pSkills.get(index))
								{
									return cg;
								}
							}
						}
						
					}
				}
				break;
			case 4:// equip
				if (cg.equips != null)
				{
					for (List<Equip> es : cg.equips)
					{
						if (es != null)
						{
							for (Equip e : es)
							{
								if (e == equips.get(index))
								{
									return cg;
								}
							}
						}
						
					}
				}
				break;
		}
		return null;
	}
	
	/**
	 * 获取指定类型指定索引的物品 lt@2014-1-13 下午08:02:58
	 * 
	 * @param pi
	 * @param eType
	 * @param index
	 *            绝对索引
	 * @return
	 */
	public static Object getObject(PlayerInfo pi, int eType, int index)
	{
		switch (eType)
		{
			case 1:
				return pi.getCards().get(index);
			case 2:
				return pi.getSkills().get(index);
			case 3:
				return pi.getPassiveSkillls().get(index);
			case 4:
				return pi.getEquips().get(index);
			case 5:
				return pi.getItems().get(index);
		}
		return null;
	}
	
	/**
	 * 创建PackElement,用于下发客户端 lt@2014-1-13 下午08:30:25
	 * 
	 * @param i
	 *            绝对索引
	 * @param o
	 *            原始对象
	 * @return
	 */
	public static PackElement createPackElement(int i, Object o, PlayerInfo pi)
	{
		PackElement pe = new PackElement();
		if (o instanceof Card)
		{
			Card c = (Card) o;
			CardGroup cg = getCardGroup(pi, 1, i);
			int use = 0;
			int skillId = 0;
			int skillLevel = 0;
			List<Integer> pSkillIds = null;
			List<String> equipInfos = null;
			if (cg != null)
			{
				use = 1;
				int pos = cg.getPos(c);
				Skill s = cg.getSkill(pos);
				if (s != null)
				{
					skillId = s.getSkillId();
					skillLevel = s.getLevel();
				}
				List<PassiveSkill> pSkills = cg.getPassiveSkills(pos);
				if (pSkills != null && pSkills.size() > 0)
				{
					pSkillIds = new ArrayList<Integer>();
					for (int j = 0; j < pSkills.size(); j++)
					{
						PassiveSkill p = pSkills.get(j);
						if (p != null)
						{
							pSkillIds.add(p.getPassiveSkillId());
						}
					}
				}
				List<Equip> equips = cg.getEquips(pos);
				if (equips != null)
				{
					equipInfos = new ArrayList<String>();
					for (int k = 0; k < equips.size(); k++)
					{
						Equip e = equips.get(k);
						equipInfos.add(e.getEquipId() + "-" + e.getLevel());
					}
				}
			}
			if (skillId == 0)
			{
				CardData cd = CardData.getData(c.getCardId());
				skillId = cd.basicskill;
				skillLevel = 1;
			}
			int battlePower = getCardPower(pi, c, skillId, skillLevel, pSkillIds, equipInfos, cg, use == 1);
			pe.setCard(i, c.getCardId(), c.getCreateTime(), c.getLevel(), c.getCurExp(), c.getBreakNum(), use, skillId, battlePower, c.getBreakType(), c.getNewType());
		}
		else if (o instanceof Skill)
		{
			Skill s = (Skill) o;
			pe.setSkill(i, s.getSkillId(), s.getLevel(), s.getCreateTime(), s.getCurExp(), Statics.isInCardGroup(pi, 2, i) ? 1 : 0, s.getNewType());
		}
		else if (o instanceof PassiveSkill)
		{
			PassiveSkill p = (PassiveSkill) o;
			pe.setPSkill(i, p.getPassiveSkillId(), p.getCreateTime(), Statics.isInCardGroup(pi, 3, i) ? 1 : 0, p.getCurExp(), p.getNewType());
		}
		else if (o instanceof Equip)
		{
			Equip e = (Equip) o;
			pe.setEquip(i, e.getEquipId(), e.getCreateTime(), e.getLevel(), Statics.isInCardGroup(pi, 4, i) ? 1 : 0, e.getNewType());
		}
		else if (o instanceof Item)
		{
			Item item = (Item) o;
			pe.setItem(i, item.getItemId(), item.getPile(), item.getNewType());
		}
		return pe;
	}
	
	/** 迷宫掉落 **/
	public static String[] mazeDropThings(MazeBattleData mbData, CardGroup cg, int itemID)
	{
		int bestProAdd = getTalentProAdd(cg);
		int random = Random.getNumber(0, Constant.TotalDropPro);
		random = (int) (random / Cache.getMazeDropMul());
		random -= bestProAdd;
		String[] list = new String[1];
		// 从全局掉落里得
		/**
		 * card,num item,num skill,num pskill,num equip,num
		 * **/
		List<EventDrop> eds = Cache.eventDropsForMaze;
		if (eds != null)
		{
			for (EventDrop ed : eds)
			{
				if (list[0] == null)
				{
					for (String s : ed.getRewards())
					{
						if (s != null)
						{
							String[] ss = s.split("&");
							int type = StringUtil.getInt(ss[0]);
							int id = StringUtil.getInt(ss[1]);
							int num = StringUtil.getInt(ss[2]);
							int pro = StringUtil.getInt(ss[3]);
							if (random < pro)
							{
								list[0] = type + "-" + id + "," + num;
								break;
							}
							random -= pro;
						}
					}
				}
				else
				{
					break;
				}
			}
		}
		if (list[0] != null)
		{
			return list;
		}
		// 从迷宫得数据
		MazeProbabilityData mpData = MazeProbabilityData.getMpData(mbData.map, mbData.type);
		if (mpData != null)
		{
			List<String> drop_pro = new ArrayList<String>();
			if (itemID != 0 && mbData.type == 3)// 许愿功能的随机掉落
			{
				WishingData wd = WishingData.getWishingDataByDrop(mbData.map, itemID);
				drop_pro.add(wd.dropid + "-" + wd.probability + "-" + 1);
				int pro = wd.probability;
				for (int i = 0; i < mpData.drop_pro.size(); i++)
				{
					String[] temp = mpData.drop_pro.get(i).split("-");
					if (pro + StringUtil.getInt(temp[1]) >= Constant.TotalDropPro)
					{
						
						drop_pro.add(temp[0] + "-" + (Constant.TotalDropPro - pro) + "-" + temp[2]);
						pro = Constant.TotalDropPro;
					}
					else
					{
						drop_pro.add(mpData.drop_pro.get(i));
					}
				}
			}
			else
			{
				drop_pro = mpData.drop_pro;
			}
			int st = 0;
			int end = 0;
			for (int i = 0; i < drop_pro.size(); i++)
			{
				String[] dropPros = drop_pro.get(i).split("-");
				end = Integer.parseInt(dropPros[1]) + st;
				if (random >= st && random < end)
				{
					int dropId = Integer.parseInt(dropPros[0]);
					List<MazeSkillDropData> msdDatas = MazeSkillDropData.getRandomMazeSkillDropData(dropId);
					int st1 = 0;
					int end1 = 0;
					int randnum1 = Random.getNumber(0, 1000);
					for (int j = 0; j < msdDatas.size(); j++)
					{
						end1 = msdDatas.get(j).probability + st1;
						if (randnum1 > st1 && randnum1 <= end1)
						{
							list[0] = msdDatas.get(j).type + "-" + msdDatas.get(j).skillID;
							break;
						}
						st1 = end1;
					}
					break;
				}
				st = end;
			}
		}
		return list;
	}
	
	/**
	 * 校验掉落物品中的各种卡牌的个数
	 * 
	 * @param ds
	 * @param pi
	 * @return
	 */
	public static boolean checkPackage(String[] ds, PlayerInfo pi)
	{
		int cardNum = 0;
		int skillNum = 0;
		int equipNum = 0;
		int itemNum = 0;
		int passiveSkillNum = 0;
		for (String drop : ds)
		{
			if (drop == null || "".equals(drop))
			{
				continue;
			}
			String[] ss = drop.split("-");
			int type = StringUtil.getInt(ss[0]);
			switch (type)
			{
				case 1:// item
					itemNum++;
					break;
				case 2:// equip
					equipNum++;
					break;
				case 3:// card
					cardNum++;
					break;
				case 4:// skill
					skillNum++;
					break;
				case 5:// passiveSkill
					passiveSkillNum++;
					break;
			}
		}
		int packageNum = getPackageNum(pi);
		if (cardNum != 0 && pi.getCards().size() >= packageNum)
		{
			return false;
		}
		else if (skillNum != 0 && pi.getSkills().size() >= packageNum)
		{
			return false;
		}
		else if (passiveSkillNum != 0 && pi.getPassiveSkillls().size() >= packageNum)
		{
			return false;
		}
		else if (equipNum != 0 && pi.getPassiveSkillls().size() >= packageNum)
		{
			return false;
		}
		else if (itemNum != 0 && pi.getItems().size() >= packageNum)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * 获取背包容量 lt@2014-6-20 上午10:55:34
	 * 
	 * @param pi
	 * @return
	 */
	public static int getPackageNum(PlayerInfo pi)
	{
		BagCostData bd = BagCostData.getData(pi.player.getBuyBagTimes());
		if (bd == null)
		{
			return 0;
		}
		return bd.number1;
	}
	
	/**
	 * 获取奖励 lt@2014-3-25 下午05:39:13
	 * 
	 * @param type
	 *            1item,2equip,3card,4skill,5pskill
	 * @param award
	 * @param pi
	 */
	public static void getReward(int type, String award, PlayerInfo pi)
	{
		switch (type)
		{
			case 1:// item
				String[] str = award.split(",");
				pi.addItem(StringUtil.getInt(str[0]), StringUtil.getInt(str[1]));
				break;
			case 2:// equip
				String[] ss = award.split(",");
				int equipId = StringUtil.getInt(ss[0]);
				int num = StringUtil.getInt(ss[1]);
				for (int k = 0; k < num; k++)
				{
					pi.addEquip(equipId);
				}
				break;
			case 3:// card
				str = award.split(",");
				for (int j = 0; j < StringUtil.getInt(str[1]); j++)
				{
					pi.addCard(StringUtil.getInt(str[0]), 1);
				}
				
				break;
			case 4:// skill
				str = award.split(",");
				for (int j = 0; j < StringUtil.getInt(str[1]); j++)
				{
					pi.addSkill(StringUtil.getInt(str[0]), 1);
				}
				
				break;
			case 5:// ps
				str = award.split(",");
				for (int j = 0; j < StringUtil.getInt(str[1]); j++)
				{
					pi.addPassiveSkill(StringUtil.getInt(str[0]));
				}
				break;
			case 6:// 金币
				pi.player.addGold(StringUtil.getInt(award));
				break;
			case 7:// 人物经验
				pi.player.addExp(StringUtil.getInt(award));
				break;
			case 8:// 水晶
				pi.player.addCrystal(StringUtil.getInt(award));
				break;
			case 9:// 符文值
				pi.player.addRuneNum(StringUtil.getInt(award));
				break;
			case 10:// 体力
				pi.player.addPower(StringUtil.getInt(award), false);
				break;
			case 11:// 友情值
				pi.player.addFriendValue(StringUtil.getInt(award));
				break;
			
		}
	}
	
	public static void getReward(int type, int rewardId, int number, PlayerInfo pi)
	{
		switch (type)
		{
			case 1:// item
				pi.addItem(rewardId, number);
				break;
			case 2:// equip
				for (int k = 0; k < number; k++)
				{
					pi.addEquip(rewardId);
				}
				break;
			case 3:// card
				for (int k = 0; k < number; k++)
				{
					pi.addCard(rewardId, 1);
				}
				break;
			case 4:// skill
				for (int k = 0; k < number; k++)
				{
					pi.addSkill(rewardId, 1);
				}
				break;
			case 5:// ps
				for (int k = 0; k < number; k++)
				{
					pi.addPassiveSkill(rewardId);
				}
				break;
			case 6:// gold
				pi.player.addGold(number);
				break;
			case 7:// personexp
				pi.player.addExp(number);
				break;
			case 8:// 水晶
				pi.player.addCrystal(number);
				break;
			case 9:// 符文
				pi.player.addRuneNum(number);
				break;
			case 10:// power
				pi.player.addPower(number, false);
				break;
			case 11:// friend
				pi.player.addFriendValue(number);
				break;
		}
	}
	
	/**
	 * 获取一张卡的总战斗力 lt@2014-4-14 下午05:18:41
	 * 
	 * @param pi
	 * @param c
	 * @param s
	 * @param p
	 * @param equipInfos
	 * @param cg
	 * @return
	 */
	public static int getCardPower(PlayerInfo pi, Card c, int skillId, int skillLevel, List<Integer> pSkillIds, List<String> equipInfos, CardGroup cg, boolean calRune)
	{
		// ==卡牌自身战力==//
		float power1 = getCardSelfMaxAtk(c.getCardId(), c.getLevel(), c.getBreakNum()) * PowerData.getMul(1) + getCardSelfMaxDef(c.getCardId(), c.getLevel(), c.getBreakNum()) * PowerData.getMul(2) + getCardSelfMaxHp(c.getCardId(), c.getLevel(), c.getBreakNum()) * PowerData.getMul(3);
		// ==装备+符文战力==//
		int atk = 0;
		int def = 0;
		int hp = 0;
		if (equipInfos != null)
		{
			for (String equip : equipInfos)
			{
				String[] ss = equip.split("-");
				EquipData ed = EquipData.getData(StringUtil.getInt(ss[0]));
				if (ed.type == 1)
				{
					atk += getEquipValue(ed, StringUtil.getInt(ss[1]));
				}
				else if (ed.type == 2)
				{
					def += getEquipValue(ed, StringUtil.getInt(ss[1]));
				}
				else if (ed.type == 3)
				{
					hp += getEquipValue(ed, StringUtil.getInt(ss[1]));
				}
			}
		}
		if (calRune)
		{
			atk += getRuneValue(pi.player.getRuneId(), 1);
			def += getRuneValue(pi.player.getRuneId(), 2);
			hp += getRuneValue(pi.player.getRuneId(), 3);
		}
		float power2 = atk * PowerData.getMul(1) + def * PowerData.getMul(2) + hp * PowerData.getMul(3);
		// ==主动技能==//
		float power3 = 0;
		CardData cd = CardData.getData(c.getCardId());
		SkillData sd = SkillData.getData(skillId);
		int value = SkillPropertyData.getProperty(sd.type, skillLevel, sd.star);
		if (sd.type == 1 || sd.type == 3)
		{
			power3 = value * PowerData.getMul(1);
		}
		else
		{
			power3 = value * PowerData.getMul(2) * PowerData.getMul(4);
		}
		// ==天赋==//
		CardPropertyData cpd = CardPropertyData.getData(c.getLevel());
		float power4 = cd.talentpower / 5 * cpd.atk * PowerData.getMul(1) + cd.talentpower / 5 * cpd.def * PowerData.getMul(2) + cd.talentpower / 5 * cpd.hp * PowerData.getMul(3);
		// ==被动技能==//
		float power5 = 0;
		if (pSkillIds != null && pSkillIds.size() > 0)
		{
			for (int psid : pSkillIds)
			{
				PassiveSkillData pd = PassiveSkillData.getData(psid);
				if (pd != null)
				{
					switch (pd.type)
					{
						case 1:
							power5 += pd.numbers * PowerData.getMul(1);
							break;
						case 2:
							power5 += pd.numbers * PowerData.getMul(2);
							break;
						case 3:
							power5 += pd.numbers * PowerData.getMul(3);
							break;
						case 6:
						case 7:
						case 8:
						case 9:
							power5 += pd.numbers * PowerData.getMul(2) * PowerData.getMul(5);
							break;
					}
				}
			}
		}
		// ==当前总战力==//
		float curPower = power1 + power2 + power3 + power4 + power5;
		// ==暴击、闪避==//
		float criPower = getCriRate(c, skillId, pSkillIds, cg, pi.player.getRuneId()) * curPower * PowerData.getMul(6);
		float aviPower = getAviRate(c, skillId, pSkillIds, cg, pi.player.getRuneId()) * curPower * PowerData.getMul(7);
		criPower = criPower / 100;
		aviPower = aviPower / 100;
		// ==总战力=当前总战力+暴击率战力+闪避率战力==//
		float power = curPower + criPower + aviPower;
		return (int) power;
	}
	
	public static boolean isTodaySign(PlayerInfo pi)
	{
		String curDate = StringUtil.getDate(System.currentTimeMillis());
		String lastSignDate = "";
		String lastSignDateTime = pi.player.getLastSignTime();
		if (lastSignDateTime != null && !"".equals(lastSignDateTime))
		{
			lastSignDate = lastSignDateTime.substring(0, 10);
		}
		return curDate.equals(lastSignDate);
	}
	
	public static int getNextInviteFriendCost(int playerLevel, int curInviteTimes)
	{
		if (curInviteTimes < Constant.FreeInviteFriendTimes)
		{
			return 0;
		}
		float cost = FriendOddData.getData(playerLevel).number * (curInviteTimes + 1 - Constant.FreeInviteFriendTimes);
		return (int) cost;
	}
	
	public static String[] getRaceAttris(PlayerInfo pi)
	{
		CardGroup cg = pi.getCardGroup();
		// ==获取各种族人数,中国,亚洲,希腊,北欧==//
		int[] races = { 1, 2, 3, 4 };
		int[] nums = { 0, 0, 0, 0 };
		for (Card c : cg.cards)
		{
			if (c != null)
			{
				CardData cd = CardData.getData(c.getCardId());
				if (cd != null)
				{
					for (int k = 0; k < races.length; k++)
					{
						if (cd.race == races[k])
						{
							nums[k] = nums[k] + 1;
						}
					}
				}
			}
		}
		// ==获取人数最多的种族,如果人数相同按照：中国、亚洲、希腊、北欧取值==//
		int maxNum = 0;
		for (int k = 0; k < nums.length; k++)
		{
			if (nums[k] > maxNum)
			{
				maxNum = nums[k];
			}
		}
		// ==获取种族属性==//
		List<String> atts = new ArrayList<String>();
		for (int k = 2; k <= maxNum; k++)
		{
			RacePowerData rd = RacePowerData.getData(k);
			if (rd != null)
			{
				atts.addAll(rd.attris);
			}
		}
		String[] result = new String[atts.size()];
		for (int k = 0; k < atts.size(); k++)
		{
			result[k] = atts.get(k);
		}
		return result;
	}
	
	// 兑换的时候检查需要物品数量是否足够
	public static boolean check(PlayerInfo pInfo, int type, int id, int num)
	{
		boolean ischeck = true;
		switch (type)
		{
			case 1:// 材料
			case 2:// 装备
			case 3:
			case 4:
			case 5:
				boolean is = pInfo.checkExchangeNeedNum(type, id, num);
				if (!is)
				{
					ischeck = false;
				}
				break;
			case 6:
				// 金币
				pInfo.player.getGold();
				if (pInfo.player.getGold() < num)
				{
					ischeck = false;
				}
				break;
			case 8:
				// 钻石
				if (pInfo.player.getTotalCrystal() < num)
				{
					ischeck = false;
				}
				break;
			case 9:
				// 符文
				if (pInfo.player.getRuneNum() < num)
				{
					ischeck = false;
				}
				break;
			case 10:
				// 体力
				if (pInfo.player.getPower() < num)
				{
					ischeck = false;
				}
				break;
		}
		return ischeck;
	}
	
	/** 计算一张卡的总血量 **/
	public static int totalCardBlood(CardGroup cg,Card c, PlayerInfo pi, List<Equip> equipInfos, List<PassiveSkill> passiveSkills, Skill skill, int selfIndex, boolean is)
	{
		//maxHp=(card_hp*(1+b_hp+c_hp+f_hp)+a_hp+d_hp+e_hp+equip_hp+g_hp)//
		//card_hp为卡牌本身的hp；//
 		//equip_hp为装备附加的hp；//
		//a_hp为被动技能的加成系数；//
		//b_hp为卡牌自身天赋的加成系数；//
		//c_hp为队友卡牌天赋的加成系数（所有队友系数相乘后的结果）；//
		//d_hp为卡牌当前装备的主动技能的加成系数；//---
		//e_hp为符文系统提供的hp加成系数；//
		//f_hp为突破系统提供的hp加成系数；//
		//g_hp为天赋系统提供的hp加成数值；//
		CardData cData = CardData.getData(c.getCardId());
		//card_hp为卡牌本身的hp；//
		float card_hp = getCardSelfMaxHp(c.getCardId(), c.getLevel(), c.getBreakNum());
		//equip_hp为装备附加的hp；//
		float equip_hp = 0;
		if (equipInfos != null)
		{
			for (Equip equip : equipInfos)
			{
				EquipData ed = EquipData.getData(equip.getEquipId());
				if (ed.type == 3)
				{
					equip_hp += getEquipValue(ed, equip.getLevel());
				}
			}
		}
		//b_hp为卡牌自身天赋的加成系数；//
		float b_hp = getTalentB_hp(cData.talent, skill.getSkillId()) + getTalentB_hp(cData.talent2, skill.getSkillId()) + getTalentB_hp(cData.talent3, skill.getSkillId());
		//e_hp为符文系统提供的hp加成系数；//
		float e_hp = 0;
		if (is)
		{
			e_hp += getRuneValue(pi.player.getRuneId(), 3);
		}
		// 种族血量加成
		int num = getRaceMaxNum(pi);
		float raceAttMul=getRaceAtt(6,num)/100;
		float r_hp = getRaceAtt(3, num);
		//g_hp为天赋系统提供的hp加成数值；//
		float g_hp = 0;
		g_hp =  getTalentG_hp(cData.talent) + getTalentG_hp(cData.talent2) + getTalentG_hp(cData.talent3);
		//a_hp为被动技能的加成系数；//
		float a_hp = 0;
		if (passiveSkills != null && passiveSkills.size()>0)
		{
			for (int i = 0; i < passiveSkills.size(); i++)
			{
				PassiveSkill ps = passiveSkills.get(i);
				if (ps!=null)
				{
					PassiveSkillData psd = PassiveSkillData.getData(ps.getPassiveSkillId());
					if (psd != null && psd.type == 3)
					{
						a_hp += psd.numbers;
					}
				}
			}
		}
		//c_hp为队友卡牌天赋的加成系数（所有队友系数相乘后的结果）；//
		int [] teamerCardIds = getTemps(cg, 1, 0, selfIndex);
		int [] talents1 = getTemps(cg, 2, 1, selfIndex);
		int [] talents2 = getTemps(cg, 2, 2, selfIndex);
		int [] talents3 = getTemps(cg, 2, 3, selfIndex);
		float c_hp = getTalentC_hp(c.getCardId(), selfIndex, teamerCardIds, talents1) + getTalentC_hp(c.getCardId(), selfIndex, teamerCardIds, talents2) + getTalentC_hp(c.getCardId(), selfIndex, teamerCardIds, talents3);
		// 卡牌当前装备的主动技能的加成系数
		float d_hp=0;
		float maxHp=(card_hp*(1+b_hp+c_hp + raceAttMul)+a_hp+d_hp+e_hp+equip_hp+g_hp + r_hp);
		return (int) maxHp;
	}
	
	public static float getTalentG_hp(int talent)
	{
		float g_hp = 0;
		// ==天赋：8对hp加成固定数值==//
		TalentData td = TalentData.getData(talent);
		if (td != null && td.type == 8 && td.class1 == 0 && td.effect == 3)
		{
			g_hp = td.number;
		}
		return g_hp;
	}
	
	// ==获取种族属性:1攻击数值,2防御数值,3生命数值,4攻击百分比,5防御百分比,6生命百分比,7暴击率,8闪避率==//
	public static float getRaceAtt(int attType, int number)
	{
		RacePowerData rpData = RacePowerData.getData(number);
		if (rpData == null)
		{
			return 0;
		}
		float result = 0;
		for (String attri : rpData.attris)
		{
			String[] ss = attri.split("-");
			int type = StringUtil.getInt(ss[0]);
			int num = StringUtil.getInt(ss[1]);
			if (type == attType)
			{
				result += num;
			}
		}
		return result;
	}
	
	// ==队友天赋加成血量==/
	private static float getTalentC_hp(int cardId, int selfIndex, int[] teamerCardIds, int[] talents)
	{
		float c_hp = 0;
		CardData cd = CardData.getData(cardId);
		if (selfIndex != -1)
		{
			// ==队友天赋加成:5对本方单位==//
			for (int k = 0; k < teamerCardIds.length; k++)
			{
				int cId = teamerCardIds[k];
				if (cId != 0)
				{
					CardData cdTemp = CardData.getData(cId);
					int talent = talents[k];
					if (talent == 0)
					{
						continue;
					}
					TalentData td2 = TalentData.getData(talent);
					if (td2 != null && (td2.type == 5 || td2.type == 51) && (td2.effect == 3 || td2.effect == 7))
					{
						switch (td2.class1)
						{
							case 1:// 本种族卡牌//
								if (cd.race == cdTemp.race)
								{
									if (td2.number > c_hp)
									{
										c_hp = td2.number;
									}
								}
								break;
							case 2:// 固定前排//
								if (selfIndex < 3)
								{
									if (td2.number > c_hp)
									{
										c_hp = td2.number;
									}
								}
								break;
							case 3:// 固定后排//
								if (selfIndex >= 3)
								{
									if (td2.number > c_hp)
									{
										c_hp = td2.number;
									}
								}
								break;
							case 4:// 前排对位//
								if (selfIndex + 3 == k)
								{
									if (td2.number > c_hp)
									{
										c_hp = td2.number;
									}
								}
								break;
							case 5:// 所有卡牌//
								if (td2.number > c_hp)
								{
									c_hp = td2.number;
								}
								break;
							default:// 固定卡牌//
								if (td2.class1 == cd.id)
								{
									if (td2.number > c_hp)
									{
										c_hp = td2.number;
									}
								}
								break;
						}
					}
				}
			}
		}
		return c_hp;
	}
	
	// 自身天赋对技能的加成
	public static float getTalentB_hp(int talent, int skillId)
	{
		float b_hp = 0;
		SkillData sd = SkillData.getData(skillId);
		// ==天赋:1对技能类型加成==//
		TalentData td = TalentData.getData(talent);
		if (sd != null && td != null && td.type == 1 && td.class1 == sd.type && td.effect == 3)
		{
			b_hp = td.number;
		}
		// ==天赋:4对特定技能==//
		if (sd != null && td != null && td.type == 4 && td.class1 / 10 == sd.index / 10 && td.effect == 3)
		{
			b_hp = td.number;
		}
		return b_hp;
	}
	
	// 获取队友cardids[]和队友talents[] type1 1.cards,2.talents;type2
	// 1.talent1,2.talent2,3.talent3
	public static int[] getTemps(CardGroup cg, int type, int type2, int selfIndex)
	{
		int length = cg.cards.length;
		int[] temp = new int[length - 1];
		switch (type)
		{
			case 1:
				int n = 0;
				for (int i = 0; i < length; i++)
				{
					if (i != selfIndex)
					{
						Card card = cg.getCard(i);
						if (card != null)
						{
							temp[n] = card.getCardId();
							n++;
						}
					}
				}
				break; 
			
			case 2:
				int a = 0;
				for (int i = 0; i < length; i++)
				{
					if (i != selfIndex)
					{
						Card card = cg.getCard(i);
						if (card != null)
						{
							CardData cData = CardData.getData(card.getCardId());
							if (type2 == 1)
							{
								temp[a] = cData.talent;
								a++;
							}
							if (type2 == 2)
							{
								temp[a] = cData.talent2;
								a++;
							}
							if (type2 == 3)
							{
								temp[a] = cData.talent3;
								a++;
							}
						}
					}
				}
				break;
		}
		return temp;
	}
	public static int getRaceMaxNum(PlayerInfo pi)
	{
		CardGroup cg = pi.getCardGroup();
		// ==获取各种族人数,中国,亚洲,希腊,北欧==//
		int[] races = { 1, 2, 3, 4 };
		int[] nums = { 0, 0, 0, 0 };
		for (Card c : cg.cards)
		{
			if (c != null)
			{
				CardData cd = CardData.getData(c.getCardId());
				if (cd != null)
				{
					for (int k = 0; k < races.length; k++)
					{
						if (cd.race == races[k])
						{
							nums[k] = nums[k] + 1;
						}
					}
				}
			}
		}
		// ==获取人数最多的种族,如果人数相同按照：中国、亚洲、希腊、北欧取值==//
		int maxNum = 0;
		for (int k = 0; k < nums.length; k++)
		{
			if (nums[k] > maxNum)
			{
				maxNum = nums[k];
			}
		}
		return maxNum;
	}
}
