package com.begamer.card.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.begamer.card.common.Constant;
import com.begamer.card.model.dao.PrivilegeDao;
import com.begamer.card.model.pojo.Manager;
import com.begamer.card.model.pojo.User;

/**
 * 
 * @ClassName: AbstractMultiActionController
 * @Description: TODO 继承Spring MVC的MultiActionController，即在一个 Controller 中处理多个动作
 * @author gs
 * @date Nov 1, 2011 1:43:48 PM
 * 
 */
public abstract class AbstractMultiActionController extends
		MultiActionController {
	
	@Autowired
	private PrivilegeDao privilegeDao;

	/**
	 * 
	 * @Title: getUser
	 * @Description: TODO 获取当前登录用户
	 */
	public User getUser(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(
				Constant.LOGIN_USER);
		return user;
	}

	/********************** GM ****************************************	
	 * 获取当前GM登录用户
	 * 
	 * @param request
	 * @return
	 */
	public Manager getManager(HttpServletRequest request) {
		Manager manager = (Manager) request.getSession().getAttribute(
				Constant.LOGIN_USER);
		return manager;
	}

	/** 权限检查 */ 
	public boolean checkQx(int managerId) {
//		搭建环境的视频中将此修改
//		boolean is = false;
//		int n = privilegeDao.getPrivilegeByManagerId(managerId, 1);
//		if (n > 0) {
//			is = true;
//		}
		return true;
	}
}
