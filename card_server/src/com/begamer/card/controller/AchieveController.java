package com.begamer.card.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.begamer.card.cache.Cache;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.common.util.Statics;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.AchievementData;
import com.begamer.card.json.command2.AchieveResultJson;
import com.begamer.card.json.command2.AchieveRewardJson;
import com.begamer.card.log.ErrorLogger;
import com.begamer.card.log.PlayerLogger;

public class AchieveController extends AbstractMultiActionController
{
	private static Logger logger=Logger.getLogger(AchieveController.class);
	private static Logger playlogger =PlayerLogger.logger;
	private static Logger errorlogger =ErrorLogger.logger;
	
	/**领取成就奖励**/
	public ModelAndView achieveReward(HttpServletRequest request ,HttpServletResponse response)
	{
		try
		{
			//校验密钥
			ModelAndView checkResult=RequestUtil.check(request);
			if(checkResult!=null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject=RequestUtil.getJsonObject(request);
			//获取参数
			AchieveRewardJson arj=JSON.toJavaObject(jsonObject, AchieveRewardJson.class);
			AchieveResultJson arrj=new AchieveResultJson();
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(arj.playerId, request.getSession());
			if(pi!=null)
			{
				int achieveId=arj.acId;
				if(pi.player.getAchieveReward(achieveId))
				{
					//获取奖励
					AchievementData ad=AchievementData.getData(achieveId);
					if(ad !=null && ad.disable==0)
					{
						List<Integer> cardIds =new ArrayList<Integer>();
						for(String rewards:ad.reward)
						{
							String[] ss=rewards.split("-");
							int type=StringUtil.getInt(ss[0]);
							if(type==9)
							{
								type =11;
							}
							if(type ==3)
							{
								String [] temp =ss[1].split(",");
								cardIds.add(StringUtil.getInt(temp[0]));
							}
							Statics.getReward(type, ss[1], pi);
							if(cardIds.size()>0)
							{
								pi.getNewUnitSkill(cardIds);
							}
						}
					}
					else
					{
						errorlogger.info("achievedata is null");
					}
					
					playlogger.info("ID:"+pi.player.getId()+"|名称："+pi.player.getName()+"|区服："+Cache.getInstance().serverId+"|领取成就奖励："+ad.id+","+ad.name);
				}
				else
				{
					arrj.errorCode=59;
				}
				if(arrj.errorCode==0)
				{
					arrj.ac=pi.player.getAchive();
				}
			}
			else
			{
				arrj.errorCode=-3;
			}
			Cache.recordRequestNum(arrj);
			String msg=JSON.toJSONString(arrj);
			//返回结果
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e)
		{
			//捕获异常,删掉旧密钥,生成新密钥发给客户端
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey=RequestUtil.setNewKey(request);
			RequestUtil.setResult(request,newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
}
