package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class LockResultJson extends ErrorJson
{
	public int type;//是否有解锁模块,0没有,1有
	public int modeId;//解锁模块的id
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	
}
