package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class FriendRefreshJson extends BasicJson
{
	/**0打开界面,1刷新**/
	public int t;

	public int getT()
	{
		return t;
	}

	public void setT(int t)
	{
		this.t = t;
	}
	
}
