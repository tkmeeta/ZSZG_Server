package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class SaleJson extends BasicJson
{
	/**1card,2skill,3equip,4item,8passiveSkill**/
	public int type;
	/**元素格式:位置**/
	public List<Integer> list;
	
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public List<Integer> getList()
	{
		return list;
	}
	public void setList(List<Integer> list)
	{
		this.list = list;
	}
}
