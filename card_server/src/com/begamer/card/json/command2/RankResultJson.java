package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class RankResultJson extends ErrorJson
{
	/**玩家排位信息** 排名-排名奖励领取标识(0，未领取，1，已领取)-pvp奖励-挑战次数-排名奖励 **/
	public String s;//rank-rankAwardType-pvpAward-pkNum-award
	/**pk对象列表** 玩家id-玩家名称-头像-排名-战力**/
	public List<String> ss;//playerid-name-head-rank-power
	public int sAward;//总奖励
	public int sPknum;//总挑战次数
	public int cdtime;//剩余挑战时间
	public List<String> cardIds;//顺序对应ss中的玩家顺序 cardid-id-id-id-id-id
	
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public List<String> getSs() {
		return ss;
	}
	public void setSs(List<String> ss) {
		this.ss = ss;
	}
	public int getSAward() {
		return sAward;
	}
	public void setSAward(int award) {
		sAward = award;
	}
	public int getSPknum() {
		return sPknum;
	}
	public void setSPknum(int pknum) {
		sPknum = pknum;
	}
	public int getCdtime() {
		return cdtime;
	}
	public void setCdtime(int cdtime) {
		this.cdtime = cdtime;
	}
	public List<String> getCardIds() {
		return cardIds;
	}
	public void setCardIds(List<String> cardIds) {
		this.cardIds = cardIds;
	}
	
	
}
