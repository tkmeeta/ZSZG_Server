package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class MapResultJson extends ErrorJson
{
	public int m1;
	public String star1;
	public String b1;
	public String t1;
	public int m2;
	public String star2;
	public String b2;
	public String t2;
	
	public int c;//水晶
	public int d;//金币
	public int kopoint;//ko积分//
	public int koType;//0没有新的ko兑换   1有新的ko兑换
	
	public int fType;//阵容newtype(1为有新提醒，0为没有,2有提升)
	
	
	public String getStar1()
	{
		return star1;
	}
	public void setStar1(String star1)
	{
		this.star1 = star1;
	}
	public String getB1()
	{
		return b1;
	}
	public void setB1(String b1)
	{
		this.b1 = b1;
	}
	public String getStar2()
	{
		return star2;
	}
	public void setStar2(String star2)
	{
		this.star2 = star2;
	}
	public String getB2()
	{
		return b2;
	}
	public void setB2(String b2)
	{
		this.b2 = b2;
	}
	public int getM1()
	{
		return m1;
	}
	public void setM1(int m1)
	{
		this.m1 = m1;
	}
	public int getM2()
	{
		return m2;
	}
	public void setM2(int m2)
	{
		this.m2 = m2;
	}
	public String getT1()
	{
		return t1;
	}
	public void setT1(String t1)
	{
		this.t1 = t1;
	}
	public String getT2()
	{
		return t2;
	}
	public void setT2(String t2)
	{
		this.t2 = t2;
	}
	public int getC()
	{
		return c;
	}
	public void setC(int c)
	{
		this.c = c;
	}
	public int getD()
	{
		return d;
	}
	public void setD(int d)
	{
		this.d = d;
	}
	public int getKopoint()
	{
		return kopoint;
	}
	public void setKopoint(int kopoint)
	{
		this.kopoint = kopoint;
	}
	public int getKoType() {
		return koType;
	}
	public void setKoType(int koType) {
		this.koType = koType;
	}
	public int getFType() {
		return fType;
	}
	public void setFType(int fType) {
		this.fType = fType;
	}
	
}
