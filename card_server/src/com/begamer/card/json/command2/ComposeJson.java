package com.begamer.card.json.command2;


import com.begamer.card.json.BasicJson;

public class ComposeJson extends BasicJson {
	/**合成：1card , 2equip , 3item**/
	public int type;
	/**合成物品的id**/
	public int index;
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public int getIndex()
	{
		return index;
	}
	public void setIndex(int index)
	{
		this.index = index;
	}
}
