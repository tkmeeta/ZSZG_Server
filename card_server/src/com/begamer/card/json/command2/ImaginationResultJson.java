package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class ImaginationResultJson extends ErrorJson 
{
	/**进入冥想界面时背包的空余格数**/
	public int i;
	/**当前激活NPC的id**/
	public int id;
	/**玩家金币**/
	public int g;
	/**当前玩家激活的领奖id**/
	public int mid;
	/**当前玩家冥想次数**/
	public int mnum;
	
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getG() {
		return g;
	}
	public void setG(int g) {
		this.g = g;
	}
	public int getMid()
	{
		return mid;
	}
	public void setMid(int mid)
	{
		this.mid = mid;
	}
	public int getMnum()
	{
		return mnum;
	}
	public void setMnum(int mnum)
	{
		this.mnum = mnum;
	}
	
	
}
