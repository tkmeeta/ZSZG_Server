package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class MainResultJson extends ErrorJson {
	public List<String> cs;
	public List<String> s;//模块id-newtype(1为有新提醒，0为没有,2有提升)
	public int onlineT;//距离在线礼包领取剩余时间
	public int giftId;//在线礼包id
	public int cost;//充值金额
	public int type;//聚宝盆是否开启标识 1未开启，0开启
	public List<String> getCs() {
		return cs;
	}

	public void setCs(List<String> cs) {
		this.cs = cs;
	}

	public List<String> getS() {
		return s;
	}

	public void setS(List<String> s) {
		this.s = s;
	}

	public int getOnlineT() {
		return onlineT;
	}

	public void setOnlineT(int onlineT) {
		this.onlineT = onlineT;
	}

	public int getGiftId() {
		return giftId;
	}

	public void setGiftId(int giftId) {
		this.giftId = giftId;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
 	
}
