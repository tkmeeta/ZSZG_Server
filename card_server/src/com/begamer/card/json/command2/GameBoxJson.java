package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class GameBoxJson extends BasicJson 
{
	public int boxId;
	public int usetimes;
	
	public int getBoxId() {
		return boxId;
	}
	public void setBoxId(int boxId) {
		this.boxId = boxId;
	}
	public int getUsetimes() {
		return usetimes;
	}
	public void setUsetimes(int usetimes) {
		this.usetimes = usetimes;
	}
	
	
}
