package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class FriendMyApplyJson extends BasicJson
{
	//==0获取我的申请,1获取申请我的信息==//
	public int t;

	public int getT()
	{
		return t;
	}

	public void setT(int t)
	{
		this.t = t;
	}
	
}
