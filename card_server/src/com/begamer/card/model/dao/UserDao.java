package com.begamer.card.model.dao;

import com.begamer.card.common.dao.GenericDao;
import com.begamer.card.model.pojo.User;

public interface UserDao extends GenericDao<User, Integer> {

}
