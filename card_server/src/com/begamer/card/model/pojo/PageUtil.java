package com.begamer.card.model.pojo;

/**
 *@author gehaizhen
 *分页工具类
 */
public class PageUtil
{
	private int total;        //数据的总条数
	private int totalPage;    //总页数
	private int currentPage;  //当前页数
	private int pageSize;     //每页显示多少
	private int upPage;       //上一页页数
	private int downPage;     //下一页页数
	
	public PageUtil(int total,int pageSize,int currentPage){
		this.total = total;
		this.pageSize = pageSize;
		this.currentPage = currentPage;
	}

	public int getTotal()
	{
		return total;
	}

	public void setTotal(int total)
	{
		this.total = total;
	}

	public int getTotalPage()
	{
		int avg = total%pageSize;
		if(avg==0){
			return total/pageSize;
		}else{
			return total/pageSize+1;
		}
	}

	public void setTotalPage(int totalPage)
	{
		this.totalPage = totalPage;
	}

	public int getCurrentPage()
	{
		return currentPage;
	}

	public void setCurrentPage(int currentPage)
	{
		this.currentPage = currentPage;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public int getUpPage()
	{
		if(currentPage<=1){
			return currentPage;
		}else{
			return currentPage-1;
		}
	}

	public void setUpPage(int upPage)
	{
		this.upPage = upPage;
	}

	public int getDownPage()
	{
		if(currentPage>=this.getTotalPage()){
			return currentPage;
		}else{
			return currentPage+1;
		}
	}

	public void setDownPage(int downPage)
	{
		this.downPage = downPage;
	}
}
