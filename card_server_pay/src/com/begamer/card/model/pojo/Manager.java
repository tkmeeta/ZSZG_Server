package com.begamer.card.model.pojo;

public class Manager {
	
	private int id;
	
	private String name;
	
	private String password;
	
	private int serverId;
	
	private int playerId;
	
	private String createOn;
	
	public int getId()
	{

		return id;
	}
	
	public void setId(int id)
	{

		this.id = id;
	}
	
	public String getName()
	{

		return name;
	}
	
	public void setName(String name)
	{

		this.name = name;
	}
	
	public String getPassword()
	{

		return password;
	}
	
	public void setPassword(String password)
	{

		this.password = password;
	}
	
	public String getCreateOn()
	{

		return createOn;
	}
	
	public void setCreateOn(String createOn)
	{

		this.createOn = createOn;
	}

	public int getServerId()
	{
		return serverId;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public int getPlayerId()
	{
		return playerId;
	}

	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	
}
